const { getBooks, addBook, addBookList, editBooks, deleteBooks } = require("./books_management");

/* Settings - start */
const aws_auth_token = process.env.auth_token || ''
/* Settings - end */

async function runLambda(method, pathname, params, body, headers) {
  // Default status code and API response
  let statusCode = 400
  let records = {
      message: 'API is working....'
  }

  // Return for root path request
  if(pathname === '/') {
    statusCode = 200
    records = { result: records, ...params, path: 'root' }
  }

  // Get list of books
  if(method === 'GET' && pathname === '/books') {
    const {error, message, data} = await getBooks(params)
    records = {status: error ? 'Error' : 'OK', message, data }
    statusCode = error ? 404 : 200
  }

  // Add a single book
  if(method === 'POST' && pathname === '/books') {
    const {error, message, data} = await addBook(body);
    records = {status: error ? 'Error' : 'OK', message, data }
    statusCode = error ? 404 : 200
  }

  // Update single book name
  if(method === 'PATCH' && pathname === '/books') {
    const {error, message, data} = await editBooks(body)
    records = {status: error ? 'Error' : 'OK', message, data }
    statusCode = error ? 404 : 200
  }

  // Add a list of books
  if(method === 'PUT' && pathname === '/books') {
    const {error, message, data} = await addBookList(body)
    records = {status: error ? 'Error' : 'OK', message, data }
    statusCode = error ? 404 : 200
  }

  // Delete books
  if(method === 'DELETE' && pathname === '/books') {
    const {error, message, data} = await deleteBooks(params)
    records = {status: error ? 'Error' : 'OK', message, data }
    statusCode = error ? 404 : 200
  }

  return {
      statusCode: statusCode,
      body: JSON.stringify(records)
  };
}

module.exports = { runLambda }