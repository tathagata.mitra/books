const http = require('http')
const url = require('url');
const { runLambda } = require("./pages");

const server = http.createServer()
server.on('request', async (req, res) => {
    const parsedUrl = url.parse(req.url,true);

    if(req.method === 'POST' || req.method==='PUT' || req.method==='PATCH') {
        var requestBody = '';

        req.on('data', async function(data) {
            requestBody += data;
        });

        req.on('end', async function () {
            const result = await runLambda(req.method, parsedUrl.pathname, '', requestBody, req.headers)
            res.writeHead(result.statusCode, {'Content-Type': 'application/json'});
            res.write(result.body);
            res.end();
        });
    }
    else {
        const result = await runLambda(req.method, parsedUrl.pathname, parsedUrl.query, false, req.headers)
        res.writeHead(result.statusCode, {'Content-Type': 'application/json'});
        res.write(result.body);
        res.end();
    }
});

const port = process.env.PORT || 8888;
server.listen(port, () => console.log(`Listning on port ${port}.....`));