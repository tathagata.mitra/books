const https = require('follow-redirects').https;
const { v4: uuidv4 } = require('uuid');
const base64 = require('base-64');
const axios = require('axios');
const querystring = require('querystring');
const cache = require('memory-cache');

/* Settings - start */

/* Settings - end */

const validateInputParams = function(params, type) {
  // default error message
  let is_invalid = false, message = 'Invalid book details.'

  // Parse the parameters
  try{
    params = JSON.parse(params)
  }catch(e) {
    is_invalid = true
  }

  // Validate book name for both add and edit API
  if(is_invalid === false && (type === 'add' || type === 'edit')) {
    if(!params.name || params.name.trim().length < 5 || params.name.trim().length >= 50) {
      is_invalid = true
      message = 'Invalid book name.'
    }
  }

  // Validate book ID for edit API
  if(is_invalid === false && type === 'edit') {
    if(!params.id || !(parseInt(params.id) > 0)) {
      is_invalid = true
      message = 'Invalid book ID.'
    }
  }
 
  // return validation result
  return { message, error: is_invalid || !params ? true : false, data: params }
}


const saveBookToDB = async function(name) {

}

const saveItemOnDatabase = async function(name, callback) {
  // Trim book name
  name = name.trim()

  // Validate book names
  if(name && name.length < 5 && name.length > 50) 
    return { message:'Invalid book name', name, id: 0 }

  // Save book into DB
  return await callback(name)
}


/**
 * Get book list
 * @returns books
 */
async function getBooks() {
  const book_name = cache.get('name');
  return {
    message: 'Books available.',
    error: false,
    data: { book_name }
  }
}

/**
 * Add books
 * @param {*} params 
 * @returns 
 */
async function addBook(params) {
  // Run initial validation for add API
  const { error, message, data } = validateInputParams(params, 'add');
  if(error)
    return { error, message }

  cache.put('name', data.name);

  return {
    message: 'Book added successfully.',
    error: false,
    data: { book_id: 7169213935, ...data }
  }
}

async function addBookList(params) {
  params = JSON.parse(params)
  console.log(params)

  return {
    message: 'Book added successfully.',
    error: false,
    data: { params }
  }
}


/**
 * Edit books
 * @param {*} params 
 * @returns 
 */
async function editBooks(params) {
  // Run initial validation for edit API
  const { error, message, data } = validateInputParams(params, 'edit');
  if(error)
    return { error, message }

  return {
    message: 'Book information updated successfully.',
    error: false,
    data: { book_id: 716921393520, ...data }
  }
}

/**
 * Delete books
 * @returns 
 */
async function deleteBooks() {
  return {
    message: 'Book deleted.',
    error: false,
    data: { book_id: 716921393520 }
  }
}

module.exports = { getBooks, addBook, addBookList, editBooks, deleteBooks }